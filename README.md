ecs

Systems deal with the information associated with that component

## Systems

Systems contain their own event buffer, this is public.

System trait:
``` rust
trait System {
    fn subscribe(ev_m: &mut EventMaster) //Subscribe to associated lists

    fn rec_event(ev: Event); //New event to be added to the buffer
    fn new_event(ev_m: &mut EventMaster); //New event created by something happening within the system

    fn process_events();
    fn tick(); //Do yo thang this tick
}
```

### Current Systems:
+ Size

+ Render
+ Move

+ Breathe
+ Bleed

## Events

``` rust
enum MoveEvent {
    Collision(usize, usize), //Collision between two entities
    ...
}

enum Parcel {
    Move(MoveEvent),
    ...
}

```
