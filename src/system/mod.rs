pub mod spatial;

pub use super::event::*;
pub use self::spatial::*;

use serde_derive::{Serialize, Deserialize};

pub trait System {
    fn react(&mut self, events: &mut Vec<Parcel>) -> Vec<Parcel>;
    fn tick(&mut self);
}

#[derive(Deserialize, Serialize, Debug, Clone, Default)]
pub struct Entity {
    pub type_name: String,

    pub spatial: Option<Spatial>
}
