use super::*;

use std::collections::HashMap;

use serde_derive::Serialize;

static PARTITION_WIDTH:  u8 = 32;
static PARTITION_HEIGHT: u8 = 32;

///A partition is a list of entity IDs relating them to their locality.
#[derive(Deserialize, Serialize, Default, Debug)]
pub struct Partition {
    entities: Vec<usize>,
}

impl Partition {
    fn add_entity(&mut self, id: usize) {
        match self.entities.binary_search(&id) {
            Ok(pos)  => eprintln!("entity '{}' already exists in partition", id),
            Err(pos) => self.entities.insert(pos, id),
        }
    }

    fn remove_entity(&mut self, id: usize) {
        match self.entities.binary_search(&id) {
            //erase return value
            Ok(pos) => {
                self.entities.remove(pos); ()
            },
            Err(_)  => eprintln!("entity '{}' does not exist in partition", id),
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone, Default)]
pub struct Spatial {
    pub pos: (f32, f32),
    pub size: (f32, f32),

    #[serde(default)]
    pub vel: (f32, f32),

    #[serde(default)]
    pub angle: f32,
}

#[derive(Deserialize, Serialize, Default)]
pub struct SpatialSystem {
    entities: Vec<Option<Spatial>>,

    ///Maps a partition to an x and y value
    spatial_partition: HashMap<(i32, i32), Partition>,
}

impl SpatialSystem {
    #[inline]
    fn get_partition(pos: (f32, f32)) -> (i32, i32) {
        (pos.0 as i32/PARTITION_WIDTH as i32, pos.1 as i32/PARTITION_HEIGHT as i32)
    }

    #[inline]
    fn new_entity(&mut self, ent: &Entity) {
        if ent.spatial.is_some() {
            let spatial = ent.spatial.clone().unwrap();
            let pos = SpatialSystem::get_partition(spatial.pos);

            self.entities.push(Some(spatial));
            
            //If the partition doesn't exist, create a new one
            let entry = self.spatial_partition
                .entry(pos)
                .or_default();
            entry.add_entity(self.entities.len()-1);
        }else{
            self.entities.push(None);
        }
    }
}

impl System for SpatialSystem {
    fn react(&mut self, events: &mut Vec<Parcel>) -> Vec<Parcel> {
        let response = Vec::new();

        for ev in events {
            match ev {
                Parcel::General(ev) => {
                    use crate::event::general_event::GeneralEvent::*;
                    match ev {
                        NewEntity(ent) => self.new_entity(&ent),
                    }
                }
            }
        }

        response
    }
    
    fn tick(&mut self) {

    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_get_partition() {
        use super::*;
        let ret_pos = SpatialSystem::get_partition((22.0, 31.0));
        assert_eq!(ret_pos, (0, 0));

        let ret_pos = SpatialSystem::get_partition((65.0, 37.0));
        assert_eq!(ret_pos, (2, 1));

        let ret_pos = SpatialSystem::get_partition((72.0, -101.0));
        assert_eq!(ret_pos, (2, -3));
    }

    #[test]
    fn test_add_entity() {
        use super::super::*;

        let ent = Entity::default();
        let mut spatial = Spatial::default();
        spatial.pos = (22.0, 31.0);

        let mut spatial_partition = SpatialSystem::default();
        spatial_partition.new_entity(&ent);

        assert_eq!(spatial_partition.entities.len(), 1);

        let calced_pos = SpatialSystem::get_partition(spatial.pos);
        let pos = spatial_partition.spatial_partition.entry(calced_pos);

        match pos {

        }
    }
}
