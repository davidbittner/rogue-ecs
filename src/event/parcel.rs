use serde_derive::Serialize;

pub use super::general_event::*;

#[derive(Debug, Serialize)]
pub enum Parcel {
    General(GeneralEvent),
}
