use super::parcel::*;
use super::super::system::*;

use serde_derive::Serialize;

#[derive(Default, Serialize)]
pub struct EventHandler {
    event_queue: Vec<Parcel>,
}

impl EventHandler {
    pub fn new() -> EventHandler {
        EventHandler::default()
    }

    #[inline]
    pub fn new_event(&mut self, ev: Parcel) {
        self.event_queue.push(ev);
    }
}
