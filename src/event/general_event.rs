use serde_derive::Serialize;

use crate::system::Entity;

#[derive(Serialize, Debug)]
pub enum GeneralEvent {
    NewEntity(Entity)
}
