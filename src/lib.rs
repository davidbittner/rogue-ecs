#![feature(get_type_id)]

pub mod system;
pub mod event;

extern crate serde;
extern crate serde_derive;
extern crate serde_yaml;

use self::system::*;
use std::collections::HashMap;

use serde_derive::Serialize;

#[derive(Default, Serialize)]
pub struct GameState {
    events:    Vec<Parcel>,
    item_dict: HashMap<String, Entity>,

    spatial: SpatialSystem,
}

impl GameState {
    pub fn new() -> GameState {
        GameState::default()
    }

    pub fn add_entities(&mut self, json: String) -> serde_yaml::Result<()> {
        let entities: Vec<Entity> = serde_yaml::from_str(&json)?;

        for ent in entities.into_iter() {
            println!("new entity '{}' registered", ent.type_name);
            self.item_dict.insert(ent.type_name.clone(), ent);
        }

        Ok(())
    }

    fn new_entity(&mut self, type_name: String) {
        let new_entity = self.item_dict.get(&type_name);
        match new_entity {
            Some(ent) => {
                let event = Parcel::General(GeneralEvent::NewEntity((*ent).clone()));

                self.events.push(event);
            }
            _ => panic!(format!("entity '{}' does not exist", type_name)),
        }
    }

    pub fn distribute_events(&mut self) {
        let mut responses = Vec::with_capacity(self.events.len());
        
        responses.append(&mut self.spatial.react(&mut self.events));

        self.events = responses;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn tester() {
        let mut state = GameState::new();
        let string = include_str!("test.yaml");

        state.add_entities(string.to_string()).expect("failed to deserialize");
        state.new_entity("wrench".to_string());
        state.new_entity("hammer".to_string());
        state.new_entity("wrench".to_string());

        state.distribute_events();
    }
}
